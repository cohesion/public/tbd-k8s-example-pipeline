from flask import Flask
from blueprints import app_blueprint
from unittest.mock import patch

app = Flask(__name__)
app.register_blueprint(app_blueprint, url_prefix='/')
app.config.from_object('config.UnitTest')


def test_health():
    test_app = app.test_client()

    rv = test_app.get('/health')
    assert rv.status == '200 OK'


@patch('blueprints.requests')
@patch('blueprints.validate_json_schema')
def test_weather(mock_validate_json_schema, mock_requests):
    mock_requests.get.return_value.status_code = 200
    mock_validate_json_schema.return_value = True

    json_payload = {
        "location": {
            "name": "Rotterdam",
            "region": "South Holland",
            "country": "Netherlands",
            "lat": 51.92,
            "lon": 4.48,
            "tz_id": "Europe/Amsterdam",
            "localtime_epoch": 1659359758,
            "localtime": "2022-08-01 15:15"
        },
        "current": {
            "last_updated_epoch": 1659358800,
            "last_updated": "2022-08-01 15:00",
            "temp_c": 22,
            "temp_f": 71.6,
            "is_day": 1,
            "condition": {
                "text": "Partly cloudy",
                "icon": "//cdn.weatherapi.com/weather/64x64/day/116.png",
                "code": 1003
            },
            "wind_mph": 8.1,
            "wind_kph": 13,
            "wind_degree": 350,
            "wind_dir": "N",
            "pressure_mb": 1019,
            "pressure_in": 30.09,
            "precip_mm": 0,
            "precip_in": 0,
            "humidity": 53,
            "cloud": 25,
            "feelslike_c": 24.2,
            "feelslike_f": 75.6,
            "vis_km": 10,
            "vis_miles": 6,
            "uv": 6,
            "gust_mph": 9.2,
            "gust_kph": 14.8
        }
    }
    mock_requests.get.return_value.json.return_value = json_payload

    test_app = app.test_client()

    rv = test_app.get('/temp?city=Rotterdam')
    print(rv.data.decode('utf-8'))

    mock_requests.get.assert_called_once()
    mock_validate_json_schema.assert_called_once()

    assert rv.status == '200 OK'
    assert 'temp' in rv.data.decode('utf-8')
