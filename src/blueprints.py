from flask import Blueprint, current_app, jsonify
from flask import request as flask_request
import requests
from utils import validate_json_schema
from schemas import weatherapi_json_schema

app_blueprint = Blueprint('app_blueprint', __name__)


@app_blueprint.route('/health', methods=['GET'])
def health():
    """healthcheck

    Returns:
        str: Simple HTML message
    """
    return '', 200


@app_blueprint.route('/temp', methods=['GET'])
def weather():
    """Flask blueprint for /temp
    Request to https://rapidapi.com/weatherapi/api/weatherapi-com/
    """
    try:
        city = flask_request.args.get('city')

        url = current_app.config['OPENWEATHER_URL']

        querystring = {"q": city}

        headers = {
            "X-RapidAPI-Key": current_app.config['X_RAPIDAPI_KEY'],
            "X-RapidAPI-Host": current_app.config['X_RAPIDAPI_HOST']
        }

        response = requests.get(
            url, headers=headers, params=querystring)

    except Exception:
        return jsonify({'message': 'error making request to weather api'}), 500

    if response.status_code != 200:
        return jsonify({'message': 'no result'}), 404

    if not validate_json_schema(weatherapi_json_schema(), response.json()):
        return jsonify({'message': 'weather api unexpected response'}), 500

    return_payload = {
        'temp': response.json()['current']['temp_c']
    }
    return jsonify(return_payload), 200
