from os import environ


class Config():
    TESTING = False
    DEBUG = True
    FLASK_ENV = 'development'
    OPENWEATHER_URL = environ.get('OPENWEATHER_URL')
    X_RAPIDAPI_KEY = environ.get('X_RAPIDAPI_KEY')
    X_RAPIDAPI_HOST = environ.get('X_RAPIDAPI_HOST')


class UnitTest():
    TESTING = True
    DEBUG = True
    FLASK_ENV = 'development'
    OPENWEATHER_URL = "fake_url"
    X_RAPIDAPI_KEY = "fake_key"
    X_RAPIDAPI_HOST = "fake_host"
