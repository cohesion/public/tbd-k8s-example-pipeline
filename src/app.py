from flask import Flask
from flask_cors import CORS
from blueprints import app_blueprint


def create_app():
    """creates the flask app with the needed blueprints

    Args:

    Returns:
        Flask app: configured flask app that can be started
    """
    app = Flask(__name__)
    CORS(app)

    app.register_blueprint(app_blueprint)

    app.config.from_object('config.Config')
    return app


def main():
    """main function

      Expected environment variables:
    """

    app = create_app()

    app.run(host="0.0.0.0", port=8080)


if __name__ == '__main__':
    main()
