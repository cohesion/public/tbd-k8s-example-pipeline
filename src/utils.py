import logging
from jsonschema import validate


def get_logger() -> logging.Logger:
    """
    Function that will return an instance of logger

    Returns:
        logging.Logger - instance
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    return logger


def validate_json_schema(json_schema, json_payload):
    try:
        validate(instance=json_payload, schema=json_schema)
    except Exception:
        return False
    return True
