import requests
from os import environ
int_host = environ['INT_HOST']


def test_health():
    response = requests.get(int_host + '/health')
    assert response.status_code == 200
