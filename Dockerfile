FROM python:3.8

# runtime dependencies
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
    curl \
    ; \
    rm -rf /var/lib/apt/lists/*

COPY ./src /app

WORKDIR /app

RUN pip3 install --no-cache-dir -r ./requirements.txt

CMD [ "python3", "./app.py" ]
