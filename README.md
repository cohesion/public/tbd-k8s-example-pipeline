# Trunk-based development example of a containerized weather service using Kubernetes

This is an example of Trunk-based development applied to a containerized service, from Build to Deployment in Kubernetes.
The service is a simple Flask API that connects to an external RapidAPI service.

## Context
As described in (https://trunkbaseddevelopment.com), Trunk-based development is "a source-control branching model, where developers collaborate on code in a single branch called ‘trunk’, resist any pressure to create other long-lived development branches by employing documented techniques. They therefore avoid merge hell, do not break the build, and live happily ever after."

<img src="./images/feature-branch.png" width="50%" height="50%">

This repo focusses on how to practically implement TDB and Continuous Deployment on Kubernetes. Deploying a container artifact often involves versioning and pinning a specific artifact, sometimes manually. 
This kind of breaks the Build-Ship-Run flow. 

Instead, we defined the following design decisions:
- Workflow is TDB, Trunk is leading and should always be stable
- Make small changes, every change is automatically tested, build. Every artifact is automatically tested in a staging environment and deployed in the production environment
- Use ephemeral deployments for testing. There shouldn't be any impediment during building and testing, multiple developers should be able to work on the same component simultaneously.
- CI and CD process are seamlessly integrated. 


## Technology
- Gitlab CI. The CI engine has a lot features that helps to build out a dynamic CI/CD pipeline
- AWS EKS as the Kubernetes platform. The Cohesion Experience Lab provides a managed EKS service for internal use.
- AWS ECR as the container registry.
- Kustomize, which allows for dynamically altering Kubernetes manifests for a specific image version or an ephemeral Kubernetes namespace.
- Flask for the weather service
- [WeatherAPI on RapidAPI](https://rapidapi.com/weatherapi/api/weatherapi-com/) is used by the weather service to request weather information.

## The Pipeline
![](./images/pipeline.png)

There are 2 pipeline processes:
1. When there is a change in the source code, the complete CI/CD pipeline will be triggered, resulting in a production deployment. 
2. When there is a change in only the deployment code (kubernetes manifests), only the deployment jobs will be triggered.

### Change in the source code
The goal is to execute the full pipeline, based on the commit that triggered the pipeline. The image artifact will be tagged with the commit sha, and this exact artifact will be used for deployment. This is achieved by referencing the unique image digest as it is defined in the container repository. In that sense, the pipeline is a single atomic transaction.

Kustomize is a powerful way to dynamically configure a kubernetes manifest. We can use Kustomize to:
- specify the exact image digest that either is released in the current pipeline, or the image digest that relates to the 'latest' image.
- specify a temporary namespace, allowing ephemeral deployments for our integration or e2e tests.

```
    kustomize edit set image ${REGISTRY}/${CONTAINER_IMAGE}@${DIGEST}
    kustomize edit set namespace "cicd-${CI_JOB_ID}"
```

Convergence happens only in the last job: deployment to production. This job has a single concurrency, which means that only 1 pipeline at a time can deploy to production. 

### Change in the source code
When there is a change in only the deployment code (kubernetes manifests), only the deployment jobs will be triggered.
Here we refer to the image that has tag 'latest' for deployment.

### Unhappy flow
We're only as confident as our test coverage. For instance, if we have a successful release, then that release should be trustworthy. If by any chance our non-prod deployment fails, or if the deployment test fails, there is no harm done because Prod is not touched. We now can fix-forward the deployment. 

